# Timer

<img src="timer_icon.png" width="64" />
Simple implementation of a Timer in and for Rust.

# Example
## Simple delay
```rust
extern crate timer;
use std::sync::mpsc::channel;
use std::time::Duration;

let timer = timer::Timer::new();
let (tx, rx) = channel();

timer.schedule_with_delay(Duration::from_secs(3), move || {
  tx.send(()).unwrap();
}).ignore();

rx.recv().unwrap();
println!("This code has been executed after 3 seconds");
```

## Periodic example
```rust
extern crate timer;
use std::time::Duration;

let timer = timer::Timer::new();

let guard = timer.schedule_repeating(Duration::from_millis(450), || {
  println!("Callback called");
});

std::thread::sleep(Duration::from_secs(3));

drop(guard);

println!("Callback has been executed 6 times");

```
