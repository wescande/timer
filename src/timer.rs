use crate::{CallbackExecutor, Function, Guard, TimerBase};
use std::time::{Duration, Instant};

/// A timer, used to schedule execution of callbacks at a later date.
///
/// In the current implementation, each timer is executed as two
/// threads. The _Scheduler_ thread is in charge of maintaining the
/// queue of callbacks to execute and of actually executing them. The
/// _Communication_ thread is in charge of communicating with the
/// _Scheduler_ thread (which requires acquiring a possibly-long-held
/// Mutex) without blocking the caller thread.
pub struct Timer {
    base: TimerBase<Box<dyn Function + Send>>,
}

impl Timer {
    /// Create a timer.
    ///
    /// This immediately launches two threads, which will remain
    /// launched until the timer is dropped. As expected, the threads
    /// spend most of their life waiting for instructions.
    pub fn new() -> Self {
        Self {
            base: TimerBase::new(CallbackExecutor),
        }
    }

    /// As `new()`, but with a manually specified initial capacity.
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            base: TimerBase::with_capacity(CallbackExecutor, capacity),
        }
    }

    /// Schedule a callback for execution after a delay.
    ///
    /// Callbacks are guaranteed to never be called before the
    /// delay. However, it is possible that they will be called a
    /// little after the delay.
    ///
    /// If the delay is negative or 0, the callback is executed as
    /// soon as possible.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, execution is cancelled.
    ///
    /// # Performance
    ///
    /// The callback is executed on the Scheduler thread. It should
    /// therefore terminate very quickly, or risk causing delaying
    /// other callbacks.
    ///
    /// # Failures
    ///
    /// Any failure in `cb` will occur on the scheduler thread and will
    /// contaminate the Timer and the calling thread itself. You have
    /// been warned.
    ///
    /// # Example
    ///
    /// ```
    /// extern crate timer;
    /// use std::sync::mpsc::channel;
    /// use std::time::Duration;
    ///
    /// let timer = timer::Timer::new();
    /// let (tx, rx) = channel();
    ///
    /// let _guard = timer.schedule_with_delay(Duration::from_millis(500), move || {
    ///   // This closure is executed on the scheduler thread,
    ///   // so we want to move it away asap.
    ///
    ///   let _ignored = tx.send(()); // Avoid unwrapping here.
    /// });
    ///
    /// rx.recv().unwrap();
    /// println!("This code has been executed after 500 milliseconds");
    /// ```
    pub fn schedule_with_delay<F>(&self, delay: Duration, cb: F) -> Guard
    where
        F: 'static + Function + Send,
    {
        self.base.schedule_with_delay(delay, Box::new(cb))
    }

    /// Schedule a callback for execution at a given date.
    ///
    /// Callbacks are guaranteed to never be called before their
    /// date. However, it is possible that they will be called a
    /// little after it.
    ///
    /// If the date is in the past, the callback is executed as soon
    /// as possible.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, execution is cancelled.
    ///
    ///
    /// # Performance
    ///
    /// The callback is executed on the Scheduler thread. It should
    /// therefore terminate very quickly, or risk causing delaying
    /// other callbacks.
    ///
    /// # Failures
    ///
    /// Any failure in `cb` will scheduler thread and progressively
    /// contaminate the Timer and the calling thread itself. You have
    /// been warned.
    pub fn schedule_with_date<F>(&self, date: Instant, cb: F) -> Guard
    where
        F: 'static + Function + Send,
    {
        self.base.schedule_with_date(date, Box::new(cb))
    }

    /// Schedule a callback for execution once per interval.
    ///
    /// Callbacks are guaranteed to never be called before their
    /// date. However, it is possible that they will be called a
    /// little after it.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, repeat is stopped.
    ///
    ///
    /// # Performance
    ///
    /// The callback is executed on the Scheduler thread. It should
    /// therefore terminate very quickly, or risk causing delaying
    /// other callbacks.
    ///
    /// # Failures
    ///
    /// Any failure in `cb` will scheduler thread and progressively
    /// contaminate the Timer and the calling thread itself. You have
    /// been warned.
    ///
    /// # Example
    ///
    /// ```
    /// extern crate timer;
    /// use std::thread;
    /// use std::sync::{Arc, Mutex};
    /// use std::time::Duration;
    ///
    /// let timer = timer::Timer::new();
    /// // Number of times the callback has been called.
    /// let count = Arc::new(Mutex::new(0));
    ///
    /// // Start repeating. Each callback increases `count`.
    /// let guard = {
    ///   let count = count.clone();
    ///   timer.schedule_repeating(Duration::from_millis(5), move || {
    ///     *count.lock().unwrap() += 1;
    ///   })
    /// };
    ///
    /// // Sleep one second. The callback should be called ~200 times.
    /// thread::sleep(Duration::from_secs(1));
    /// let count_result = *count.lock().unwrap();
    /// assert!(190 <= count_result && count_result <= 210,
    ///   "The timer was called {} times", count_result);
    ///
    /// // Now drop the guard. This should stop the timer.
    /// drop(guard);
    /// thread::sleep(Duration::from_millis(100));
    ///
    /// // Let's check that the count stops increasing.
    /// let count_start = *count.lock().unwrap();
    /// thread::sleep(Duration::from_secs(1));
    /// let count_stop = *count.lock().unwrap();
    /// assert_eq!(count_start, count_stop);
    /// ```
    pub fn schedule_repeating<F>(&self, repeat: Duration, cb: F) -> Guard
    where
        F: 'static + Function + Send,
    {
        self.base.schedule_repeating(repeat, Box::new(cb))
    }

    /// Schedule a callback for execution at a given time, then once
    /// per interval. A typical use case is to execute code once per
    /// day at 12am.
    ///
    /// Callbacks are guaranteed to never be called before their
    /// date. However, it is possible that they will be called a
    /// little after it.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, repeat is stopped.
    ///
    ///
    /// # Performance
    ///
    /// The callback is executed on the Scheduler thread. It should
    /// therefore terminate very quickly, or risk causing delaying
    /// other callbacks.
    ///
    /// # Failures
    ///
    /// Any failure in `cb` will occur on the scheduler thread and progressively
    /// contaminate the Timer and the calling thread itself. You have
    /// been warned.
    pub fn schedule<F>(&self, date: Instant, repeat: Option<Duration>, cb: F) -> Guard
    where
        F: 'static + Function + Send,
    {
        self.base.schedule(date, repeat, Box::new(cb))
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::mpsc::channel;
    use std::sync::{Arc, Mutex};
    use std::thread;

    custom_test! {
        #[test(timeout = Duration::from_millis(2000))]
        fn test_schedule_with_delay() {
            let timer = Timer::new();
            let (tx, rx) = channel();
            let mut guards = vec![];

            // Schedule a number of callbacks in an arbitrary order, make sure
            // that they are executed in the right order.
            let mut delays = vec![100, 500, 300, 1000];
            let start = Instant::now();
            for i in delays.clone() {
                println!("Scheduling for execution in {} ms", i);
                let tx = tx.clone();
                guards.push(
                    timer.schedule_with_delay(Duration::from_millis(i), move || {
                        tx.send(i).unwrap();
                    }),
                );
            }

            delays.sort();
            println!("| msg  |  ms  | ");
            for (i, msg) in (0..delays.len()).zip(rx.iter()) {
                let elapsed: u64 = (start.elapsed().as_millis()) as u64;
                println!("| {:4} | {:4} |", msg, elapsed);
                assert_eq!(msg, delays[i]);
                assert!(
                    delays[i] <= elapsed && elapsed <= delays[i] + 10,
                    "We have waited {} seconds, expecting [{}, {}]",
                    elapsed,
                    delays[i],
                    delays[i] + 3
                );
            }

            // Now make sure that callbacks that are designed to be executed
            // immediately are executed quickly.
            let start = Instant::now();
            for i in vec![10, 0] {
                println!("Scheduling for execution in {} seconds", i);
                let tx = tx.clone();
                guards.push(timer.schedule_with_delay(Duration::from_secs(i), move || {
                    tx.send(i).unwrap();
                }));
            }

            assert_eq!(rx.recv().unwrap(), 0);
            assert!(start.elapsed() <= Duration::from_secs(1));
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(2000))]
        fn test_schedule_repeating() {
            let timer = Timer::new();

            // Schedule a number of periodic callbacks in an arbitrary order
            let delay_array: [Duration; 4] = [
                Duration::from_millis(250),
                Duration::from_millis(25),
                Duration::from_millis(500),
                Duration::from_micros(5000),
            ];
            let count_array = [
                Arc::new(Mutex::new(0)),
                Arc::new(Mutex::new(0)),
                Arc::new(Mutex::new(0)),
                Arc::new(Mutex::new(0)),
            ];
            let mut guards: [Option<Guard>; 4] = [None, None, None, None];
            let start = Instant::now();
            for i in 0..delay_array.len() {
                let cpt = count_array[i].clone();
                println!(
                    "Scheduling for execution in {:?} => {:?}",
                    i, delay_array[i]
                );
                guards[i] = Some(timer.schedule_repeating(delay_array[i], move || {
                    let mut cpt = cpt.lock().unwrap();
                    *cpt += 1;
                }));
            }
            let max = delay_array.iter().max().unwrap().clone();

            thread::sleep(max * 2);
            for i in 0..guards.len() {
                guards[i] = None;
            }
            let elapsed = start.elapsed();
            println!("Elapsed time {:?}", elapsed);
            for i in 0..delay_array.len() {
                let normal_cpt = max.as_micros() * 2 / delay_array[i].as_micros();
                assert!(normal_cpt as usize - *count_array[i].lock().unwrap() <= 1);
                // Allow a deviance of at most 1 in callback count (if period is a exact divider of main sleep, we cannot guaranteed if the last call is done before / after the drop)
            }
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(1000))]
        fn test_overload() {
            // Make sure that if timer is overload we still can communicate with
            // and ask it to stop or schedule an other task.
            // An overloaded timer is easily done by scheduling a task longer to
            // execute than it repeat interval.
            let timer = Timer::new();
            let called = Arc::new(Mutex::new(false));
            let called2 = Arc::new(Mutex::new(false));

            {
                let called = called.clone();
                // Overload timer with a task longer than repeat time
                timer.schedule(Instant::now(), Some(Duration::from_millis(10)), move || {
                    thread::sleep(std::time::Duration::from_millis(30));
                    *called.lock().unwrap() = true;
                }).ignore();
                let called2 = called2.clone();
                // ask for an other task
                timer.schedule(Instant::now(), None, move || {
                    thread::sleep(std::time::Duration::from_millis(30));
                    *called2.lock().unwrap() = true;
                }).ignore();
            }

            thread::sleep(std::time::Duration::from_millis(150));
            drop(timer);
            thread::sleep(std::time::Duration::from_millis(150));
            assert!(*called.lock().unwrap(), "Periodic task has never been called");
            assert!(*called2.lock().unwrap(), "One-time task has never been called");
            *called.lock().unwrap() = false;
            thread::sleep(std::time::Duration::from_millis(200));
            assert!(!*called.lock().unwrap(), "Periodic task was still running after timer drop");
        }
    }
}
