//! A simple timer without dependency, used to enqueue operations meant to be executed at
//! a given time, periodically or after a given delay.
//!
//! Source code is available on [gitlab].
//!
//! This project is a fork from [Yoric].
//!
//! Here a list of what is in this timer and not (yet) on [Yoric] implementation:
//! * Dependency only on std: use `std::time{Duration, Instant}` instead of `chrono`
//! * When starting a timer, there is one thread less as we do not need to implement
//!    channel to schedule task. We directly push the task on queue and notify the
//!    scheduler thread
//! * A new guard interface to give some control:
//!    * We can join on guard, to block until task has been executed
//!    * We can periodic_join, to block until task is no longer schedule
//! * Code is divided in more smaller file
//! * Some fix when timer was overload (PR to Original done)
//! * Add timeout on test (great when you wait for a dead lock)
//!
//! [gitlab]: https://gitlab.com/wescande/timer
//! [Yoric]: https://github.com/Yoric/timer.rs

#![doc(html_logo_url = "https://gitlab.com/wescande/timer/-/raw/master/timer_icon.png")]

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::sync::{mpsc::Sender, Arc, Condvar, Mutex};
use std::time::{Duration, Instant};

#[macro_use]
pub(crate) mod macros;
mod timer;
pub use timer::Timer;
mod message_timer;
pub use message_timer::MessageTimer;
mod guard;
pub use guard::GenericGuard;
pub use guard::Guard;
mod timer_base;
use timer_base::TimerBase;

/// Trait is need to implement clone on type `Fn()`
pub trait Function: Fn() {
    fn clone_boxed(&self) -> Box<dyn Function + Send>;
}

impl<T> Function for T
where
    T: 'static + Clone + Fn() + Send,
{
    fn clone_boxed(&self) -> Box<dyn Function + Send> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Function + Send> {
    fn clone(&self) -> Self {
        self.clone_boxed()
    }
}

/// An item scheduled for delayed execution.
struct Item<T>
where
    T: 'static + Clone + Send,
{
    /// The instant at which to execute.
    date: Instant,

    /// The item data.
    data: T,

    /// A mechanism to cancel execution of an item.
    guard: GenericGuard<T>,

    /// If `Some(d)`, the item must be repeated every interval of
    /// length `d`, until cancelled.
    repeat: Option<Duration>,
}
impl<T> Ord for Item<T>
where
    T: 'static + Clone + Send,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.date.cmp(&other.date).reverse()
    }
}
impl<T> PartialOrd for Item<T>
where
    T: 'static + Clone + Send,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.date.partial_cmp(&other.date).map(|ord| ord.reverse())
    }
}
impl<T> Eq for Item<T> where T: 'static + Clone + Send {}
impl<T> PartialEq for Item<T>
where
    T: 'static + Clone + Send,
{
    fn eq(&self, other: &Self) -> bool {
        self.date.eq(&other.date)
    }
}

/// An operation to be sent across threads.
enum Op<T>
where
    T: 'static + Clone + Send,
{
    /// Schedule a new item for execution.
    Schedule(Item<T>),

    /// Check if some item has ended execution
    CheckExecution,

    /// Stop the thread.
    Stop,
}

/// A mutex-based kind-of-channel used to communicate between the
/// Communication thread and the Scheduler thread.
struct WaiterChannel<T>
where
    T: 'static + Clone + Send,
{
    /// Pending messages.
    messages: Mutex<Vec<Op<T>>>,
    /// A condition variable used for waiting.
    condvar: Condvar,
}
impl<T> WaiterChannel<T>
where
    T: 'static + Clone + Send,
{
    fn with_capacity(cap: usize) -> Self {
        WaiterChannel {
            messages: Mutex::new(Vec::with_capacity(cap)),
            condvar: Condvar::new(),
        }
    }
    fn send(&self, op: Op<T>) {
        let mut lock = self.messages.lock().unwrap();
        if let Op::Stop = op {
            lock.clear();
        }
        lock.push(op);
        self.condvar.notify_one();
    }
}

/// A trait that allows configurable execution of scheduled item
/// on the scheduler thread.
trait Executor<T> {
    // Due to difference in use between Box<FnMut()> and most other data
    // types, this trait requires implementors to provide two implementations
    // of execute. While both of these functions execute the data item
    // they differ on whether they make an equivalent data item available
    // to the Scheduler to store in recurring schedules.
    //
    // execute() is called whenever a non-recurring data item needs
    // to be executed, and consumes the data item in the process.
    //
    // execute_clone() is called whenever a recurring data item needs
    // to be executed, and produces a new equivalent data item. This
    // function should be more or less equivalent to:
    //
    // fn execute_clone(&mut self, data : T) -> T {
    //   self.execute(data.clone());
    //   data
    // }

    fn execute(&mut self, data: T);

    fn execute_clone(&mut self, data: T) -> T;
}

/// An executor implementation for executing callbacks on the scheduler
/// thread.
struct CallbackExecutor;

impl Executor<Box<dyn Function + Send>> for CallbackExecutor {
    fn execute(&mut self, data: Box<dyn Function + Send>) {
        data();
    }

    fn execute_clone(&mut self, data: Box<dyn Function + Send>) -> Box<dyn Function + Send> {
        data();
        data
    }
}

/// An executor implementation for delivering messages to a channel.
struct DeliveryExecutor<T>
where
    T: 'static + Send,
{
    /// The channel to deliver messages to.
    tx: Sender<T>,
}

impl<T> Executor<T> for DeliveryExecutor<T>
where
    T: 'static + Send + Clone,
{
    fn execute(&mut self, data: T) {
        let _ = self.tx.send(data);
    }

    fn execute_clone(&mut self, data: T) -> T {
        let _ = self.tx.send(data.clone());
        data
    }
}

struct Scheduler<T, E>
where
    E: Executor<T>,
    T: 'static + Clone + Send,
{
    waiter: Arc<WaiterChannel<T>>,
    heap: BinaryHeap<Item<T>>,
    executor: E,
}

impl<T, E> Drop for Scheduler<T, E>
where
    E: Executor<T>,
    T: 'static + Send + Clone,
{
    /// Advertise each guard there is nothing to wait for
    fn drop(&mut self) {
        for item in self.heap.drain() {
            item.guard.execution_done();
        }
    }
}

impl<T, E> Scheduler<T, E>
where
    E: Executor<T>,
    T: 'static + Clone + Send,
{
    fn with_capacity(waiter: Arc<WaiterChannel<T>>, executor: E, capacity: usize) -> Self {
        Scheduler {
            waiter,
            executor,
            heap: BinaryHeap::with_capacity(capacity),
        }
    }

    // Iter over all heap to check if there is task that should not be execute
    // In future heap version, we will also remove item from heap
    fn check_exec(&self) {
        for item in self.heap.iter() {
            if !item.guard.should_execute() {
                item.guard.execution_done();
            }
        }
    }

    fn run(&mut self) {
        enum Sleep {
            NotAtAll,
            UntilAwakened,
            AtMost(Duration),
        }

        loop {
            let now = Instant::now();
            let mut sleep = if let Some(item) = self.heap.peek() {
                if item.date > now {
                    // First item is not ready yet, so we need to
                    // wait until it is or something happens.
                    Sleep::AtMost(item.date.duration_since(now))
                } else {
                    // At this stage, we have an item that has reached
                    // execution time. The `unwrap()` is guaranteed to
                    // succeed.
                    let item = self.heap.pop().unwrap();
                    if !item.guard.should_execute() {
                        // Execution has been cancelled, skip this item.
                        item.guard.execution_done();
                    } else {
                        // We have something to do
                        if let Some(delta) = item.repeat {
                            let data = self.executor.execute_clone(item.data);

                            // This is a repeating timer, so we need to
                            // enqueue the next call.
                            self.heap.push(Item {
                                date: item.date + delta,
                                data,
                                guard: item.guard,
                                repeat: Some(delta),
                            });
                        } else {
                            self.executor.execute(item.data);
                            item.guard.execution_done();
                        }
                    }
                    // Item has been executed or dismissed, do not sleep
                    Sleep::NotAtAll
                }
            } else {
                // Nothing to do
                Sleep::UntilAwakened
            };

            let mut lock = self.waiter.messages.lock().unwrap();
            // Pop all messages.
            for msg in lock.drain(..) {
                match msg {
                    Op::Stop => {
                        // Guards are marked has done in drop implementation
                        return;
                    }
                    Op::CheckExecution => {
                        self.check_exec();
                    }
                    Op::Schedule(item) => {
                        self.heap.push(item);
                        sleep = Sleep::NotAtAll;
                    }
                }
            }

            match sleep {
                Sleep::UntilAwakened => {
                    let _ = self.waiter.condvar.wait(lock);
                }
                Sleep::AtMost(delay) => {
                    let _ = self.waiter.condvar.wait_timeout(lock, delay);
                }
                Sleep::NotAtAll => {}
            }
        }
    }
}
