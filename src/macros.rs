#[cfg(test)]
macro_rules! custom_test {
    (
        #[test(timeout = $timeout:expr)]
        $( #[$meta:meta] )*
        fn $fname:ident $($rest:tt)*
    ) => (
    #[test]
    $( #[$meta] )*
    fn $fname ()
    {
        let (done_tx, done_rx) = ::std::sync::mpsc::channel();
        let handle =::std::thread::Builder::new()
            .name(
                concat!(module_path!(), "::", stringify!($fname))
                .splitn(2, "::").nth(1).unwrap()
                .into()
            )
            .spawn(move || {
                {
                    fn $fname $($rest)*
                        $fname();
                }
                let _ = done_tx.send(());
            })
            .unwrap();

        match done_rx.recv_timeout({
            use ::std::time::*;
            $timeout
        }) {
            | Err(::std::sync::mpsc::RecvTimeoutError::Timeout) => {
                panic!("Test took too long");
            },
            | _ => if let Err(err) = handle.join() {
                ::std::panic::resume_unwind(err);
            },
        }
    }
    );

    (
        $($tt:tt)*
    ) => (
    $($tt)*
    );
}
