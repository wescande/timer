use crate::{DeliveryExecutor, GenericGuard, TimerBase};
use std::sync::mpsc::Sender;
use std::time::{Duration, Instant};

/// A timer, used to schedule delivery of messages at a later date.
///
/// In the current implementation, each timer is executed as two
/// threads. The _Scheduler_ thread is in charge of maintaining the
/// queue of messages to deliver and of actually delivering them. The
/// _Communication_ thread is in charge of communicating with the
/// _Scheduler_ thread (which requires acquiring a possibly-long-held
/// Mutex) without blocking the caller thread.
///
/// Similar functionality could be implemented using the generic Timer
/// type, however, using MessageTimer has two performance advantages
/// over doing so. First, MessageTimer does not need to heap allocate
/// a closure for each scheduled item, since the messages to queue are
/// passed directly. Second, MessageTimer avoids the dynamic dispatch
/// overhead associated with invoking the closure functions.
pub struct MessageTimer<T>
where
    T: 'static + Send + Clone,
{
    base: TimerBase<T>,
}

impl<T> MessageTimer<T>
where
    T: 'static + Send + Clone,
{
    /// Create a message timer.
    ///
    /// This immediately launches two threads, which will remain
    /// launched until the timer is dropped. As expected, the threads
    /// spend most of their life waiting for instructions.
    pub fn new(tx: Sender<T>) -> Self {
        Self {
            base: TimerBase::new(DeliveryExecutor { tx }),
        }
    }

    /// As `new()`, but with a manually specified initial capacity.
    pub fn with_capacity(tx: Sender<T>, capacity: usize) -> Self {
        Self {
            base: TimerBase::with_capacity(DeliveryExecutor { tx }, capacity),
        }
    }

    /// Schedule a message for delivery after a delay.
    ///
    /// Messages are guaranteed to never be delivered before the
    /// delay. However, it is possible that they will be delivered a
    /// little after the delay.
    ///
    /// If the delay is negative or 0, the message is delivered as
    /// soon as possible.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, delivery is cancelled.
    ///
    ///
    /// # Example
    ///
    /// ```
    /// extern crate timer;
    /// use std::sync::mpsc::channel;
    /// use std::time::Duration;
    ///
    /// let (tx, rx) = channel();
    /// let timer = timer::MessageTimer::new(tx);
    /// let _guard = timer.schedule_with_delay(Duration::from_secs(3), 3);
    ///
    /// rx.recv().unwrap();
    /// println!("This code has been executed after 3 seconds");
    /// ```
    pub fn schedule_with_delay(&self, delay: Duration, msg: T) -> GenericGuard<T> {
        self.base.schedule_with_delay(delay, msg)
    }

    /// Schedule a message for delivery at a given date.
    ///
    /// Messages are guaranteed to never be delivered before their
    /// date. However, it is possible that they will be delivered a
    /// little after it.
    ///
    /// If the date is in the past, the message is delivered as soon
    /// as possible.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, delivery is cancelled.
    ///
    pub fn schedule_with_date(&self, date: Instant, msg: T) -> GenericGuard<T> {
        self.base.schedule_with_date(date, msg)
    }

    /// Schedule a message for delivery once per interval.
    ///
    /// Messages are guaranteed to never be delivered before their
    /// date. However, it is possible that they will be delivered a
    /// little after it.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, repeat is stopped.
    ///
    ///
    /// # Performance
    ///
    /// The message is cloned on the Scheduler thread. Cloning of
    /// messages should therefore succeed very quickly, or risk
    /// delaying other messages.
    ///
    /// # Failures
    ///
    /// Any failure in cloning of messages will occur on the scheduler thread
    /// and will contaminate the Timer and the calling thread itself. You have
    /// been warned.
    ///
    /// # Example
    ///
    /// ```
    /// extern crate timer;
    /// use std::sync::mpsc::channel;
    /// use std::time::Duration;
    ///
    /// let (tx, rx) = channel();
    /// let timer = timer::MessageTimer::new(tx);
    ///
    /// // Start repeating.
    /// let guard = timer.schedule_repeating(Duration::from_millis(5), 0);
    ///
    /// let mut count = 0;
    /// while count < 5 {
    ///   let _ = rx.recv();
    ///   println!("Prints every 5 milliseconds");
    ///   count += 1;
    /// }
    /// ```
    pub fn schedule_repeating(&self, repeat: Duration, msg: T) -> GenericGuard<T> {
        self.base.schedule_repeating(repeat, msg)
    }

    /// Schedule a message for delivery at a given time, then once
    /// per interval. A typical use case is to execute code once per
    /// day at 12am.
    ///
    /// Messages are guaranteed to never be delivered before their
    /// date. However, it is possible that they will be delivered a
    /// little after it.
    ///
    /// This method returns a `Guard` object. If that `Guard` is
    /// dropped, repeat is stopped.
    ///
    /// # Performance
    ///
    /// The message is cloned on the Scheduler thread. Cloning of
    /// messages should therefore succeed very quickly, or risk
    /// delaying other messages.
    ///
    /// # Failures
    ///
    /// Any failure in cloning of messages will occur on the scheduler thread
    /// and will contaminate the Timer and the calling thread itself. You have
    /// been warned.
    pub fn schedule(&self, date: Instant, repeat: Option<Duration>, msg: T) -> GenericGuard<T> {
        self.base.schedule(date, repeat, msg)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::mpsc::channel;

    struct NoCloneMessage;

    impl Clone for NoCloneMessage {
        fn clone(&self) -> Self {
            panic!("TestMessage should not be cloned");
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(1000))]
        fn test_message_timer() {
            let (tx, rx) = channel();
            let timer = MessageTimer::new(tx);

            let mut delays = vec![400, 300, 100, 500, 200];
            for delay in delays.clone() {
                timer
                    .schedule_with_delay(Duration::from_millis(delay), delay)
                    .ignore();
            }

            delays.sort();
            for delay in delays {
                assert_eq!(rx.recv().unwrap(), delay);
            }
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(500))]
        fn test_no_clone() {
            // Make sure that, if no schedule is supplied to a MessageTimer
            // the message instances are not cloned.
            let (tx, rx) = channel();
            let timer = MessageTimer::new(tx);
            timer
                .schedule_with_delay(Duration::from_millis(0), NoCloneMessage)
                .ignore();
            timer
                .schedule_with_delay(Duration::from_millis(0), NoCloneMessage)
                .ignore();

            for _ in 0..2 {
                let _ = rx.recv();
            }
        }
    }
}
