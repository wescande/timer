use crate::{Executor, GenericGuard, Item, Op, Scheduler, WaiterChannel};
use std::{
    sync::Arc,
    thread,
    time::{Duration, Instant},
};

/// Shared coordination logic for timer threads.
pub(crate) struct TimerBase<T>
where
    T: 'static + Send + Clone,
{
    /// Sender used to communicate with the _Communication_ thread. In
    /// turn, this thread will send
    // tx: Sender<Op<T>>,
    waiter: Arc<WaiterChannel<T>>,
}

impl<T> Drop for TimerBase<T>
where
    T: 'static + Send + Clone,
{
    /// Stop the timer threads.
    fn drop(&mut self) {
        self.waiter.send(Op::Stop);
    }
}

impl<T> TimerBase<T>
where
    T: 'static + Send + Clone,
{
    /// Create a timer base.
    ///
    /// This immediately launches two threads, which will remain
    /// launched until the timer is dropped. As expected, the threads
    /// spend most of their life waiting for instructions.
    pub(crate) fn new<E>(executor: E) -> Self
    where
        E: 'static + Executor<T> + Send,
    {
        Self::with_capacity(executor, 32)
    }

    /// As `new()`, but with a manually specified initial capacity.
    pub(crate) fn with_capacity<E>(executor: E, capacity: usize) -> Self
    where
        E: 'static + Executor<T> + Send,
    {
        let waiter_send = Arc::new(WaiterChannel::with_capacity(capacity));
        let waiter_recv = waiter_send.clone();

        thread::Builder::new()
            .name("Timer thread".to_owned())
            .spawn(move || {
                let mut scheduler = Scheduler::with_capacity(waiter_recv, executor, capacity);
                scheduler.run()
            })
            .unwrap();
        Self { waiter: waiter_send }
    }

    pub(crate) fn schedule_with_delay(&self, delay: Duration, data: T) -> GenericGuard<T> {
        self.schedule_with_date(Instant::now() + delay, data)
    }

    pub(crate) fn schedule_with_date(&self, date: Instant, data: T) -> GenericGuard<T> {
        self.schedule(date, None, data)
    }

    pub(crate) fn schedule_repeating(&self, repeat: Duration, data: T) -> GenericGuard<T> {
        self.schedule(Instant::now() + repeat, Some(repeat), data)
    }

    pub(crate) fn schedule(
        &self,
        date: Instant,
        repeat: Option<Duration>,
        data: T,
    ) -> GenericGuard<T> {
        let guard = GenericGuard::new(self.waiter.clone(), repeat != None);
        self.waiter.send(Op::Schedule(Item {
            date,
            data,
            guard: guard.clone(),
            repeat,
        }));
        guard
    }
}
