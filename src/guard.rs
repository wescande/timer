use crate::{Function, Op, WaiterChannel};
use std::sync::atomic::{AtomicBool, Ordering as AtomicOrdering};
use std::sync::{Arc, Condvar, Mutex};

/// An already defined GenericGuard for simple timer
pub type Guard = GenericGuard<Box<dyn Function + Send>>;

#[derive(Clone)]
/// A value scoping a schedule. When this value is dropped, the
/// schedule is cancelled unless ignore has been called.
pub struct GenericGuard<T>
where
    T: 'static + Send + Clone,
{
    should_execute: Arc<AtomicBool>,
    execution_done: Arc<DONE>,
    ignore_drop: bool,
    waiter: Arc<WaiterChannel<T>>,
    repeat: bool,
}
struct DONE {
    state: Mutex<bool>,
    condvar: Condvar,
}

impl<T> GenericGuard<T>
where
    T: 'static + Send + Clone,
{
    pub(crate) fn new(waiter: Arc<WaiterChannel<T>>, repeat: bool) -> Self {
        Self {
            should_execute: Arc::new(AtomicBool::new(true)),
            execution_done: Arc::new(DONE {
                state: Mutex::new(false),
                condvar: Condvar::new(),
            }),
            ignore_drop: false,
            waiter,
            repeat,
        }
    }
    pub(crate) fn should_execute(&self) -> bool {
        self.should_execute.load(AtomicOrdering::Relaxed)
    }
    pub(crate) fn execution_done(&self) {
        let mut lock = self.execution_done.state.lock().unwrap();
        *lock = true;
        self.execution_done.condvar.notify_one();
    }

    /// Block until item is successfully execute
    /// Panic If guard correspond to a periodic item
    /// For periodic see
    /// [`GenericGuard::periodic_join`](struct.GenericGuard.html#method.periodic_join) method.
    ///
    /// # Example
    ///
    /// ```
    /// extern crate timer;
    /// use std::time::Duration;
    ///
    /// println!("Start");
    /// timer::Timer::new().schedule_with_delay(Duration::from_millis(2000), move || { () }).join();
    /// println!("This code has been executed after 2 seconds");
    /// ```
    pub fn join(&self) {
        assert!(
            !self.repeat,
            "Joining a periodic guard result in a dead lock"
        );
        let lock = self.execution_done.state.lock().unwrap();
        if !*lock {
            let _ = self.execution_done.condvar.wait(lock);
        }
    }

    /// Discard all future item schedule.
    /// Block until periodic item is no longer on scheduler thread
    ///
    /// Warning, if item was scheduled with BOTH delay and date,
    /// this does **NOT** guarantee item to be executed at all
    /// as opposite of
    /// [`GenericGuard::join`](struct.GenericGuard.html#method.join) method.
    pub fn periodic_join(&self) {
        self.should_execute.store(false, AtomicOrdering::Relaxed);
        self.waiter.send(Op::CheckExecution);
        let lock = self.execution_done.state.lock().unwrap();
        if !*lock {
            let _ = self.execution_done.condvar.wait(lock);
        }
    }

    /// Ignores the guard, preventing it from disabling the scheduled
    /// item. This can be used to avoid maintaining a Guard handle
    /// for items that will never be cancelled.
    pub fn ignore(mut self) {
        self.ignore_drop = true;
    }
}
impl<T> Drop for GenericGuard<T>
where
    T: 'static + Send + Clone,
{
    /// Cancel a schedule.
    fn drop(&mut self) {
        if !self.ignore_drop {
            self.should_execute.store(false, AtomicOrdering::Relaxed)
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate std;
    use std::sync::{Arc, Mutex};
    use std::thread;
    use std::time::Duration;
    use timer::Timer;

    custom_test! {
            #[test(timeout = Duration::from_millis(1000))]
            fn test_guards() {
            println!("Testing that callbacks aren't called if the guard is dropped");
            let timer = Timer::new();
            let called = Arc::new(Mutex::new(false));

            for i in 10..100 {
                let called = called.clone();
                timer.schedule_with_delay(Duration::from_millis(i), move || {
                    *called.lock().unwrap() = true;
                });
            }

            thread::sleep(Duration::from_millis(100));
            assert_eq!(*called.lock().unwrap(), false);
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(1000))]
        fn test_guard_ignore() {
            let timer = Timer::new();
            let called = Arc::new(Mutex::new(false));

            {
                let called = called.clone();
                timer
                    .schedule_with_delay(Duration::from_millis(100), move || {
                        *called.lock().unwrap() = true;
                    })
                    .ignore();
            }

            thread::sleep(Duration::from_millis(200));
            assert_eq!(*called.lock().unwrap(), true);
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(2000))]
        fn test_drop_during_schedule() {
            let called = Arc::new(Mutex::new(0));
            let timer = Timer::new();
            {
                let called = called.clone();
                let guard = timer.schedule_with_delay(Duration::from_millis(0), move || {
                    *called.lock().unwrap() += 1;
                    thread::sleep(Duration::from_millis(700));
                    *called.lock().unwrap() += 1;
                });
                thread::sleep(Duration::from_millis(500));
                drop(guard);
            }
            thread::sleep(Duration::from_millis(1000));

            assert_eq!(*called.lock().unwrap(), 2);
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(1000))]
        fn test_join() {
            let called = Arc::new(Mutex::new(false));
            {
                let called = called.clone();
                Timer::new()
                    .schedule_with_delay(Duration::from_millis(500), move || {
                        *called.lock().unwrap() = true;
                    })
                    .join();
            }
            assert_eq!(*called.lock().unwrap(), true);
        }
    }

    custom_test! {
        #[test(timeout = Duration::from_millis(2000))]
        fn test_periodic_join() {
                let called = Arc::new(Mutex::new(0));
                let timer = Timer::new();
                {
                    let called = called.clone();
                    let guard = timer.schedule_repeating(Duration::from_millis(100), move || {
                        *called.lock().unwrap() += 1;
                    });
                    thread::sleep(Duration::from_millis(550));
                    guard.periodic_join();
                }
                thread::sleep(Duration::from_millis(500));
                assert_eq!(*called.lock().unwrap(), 5);
        }
    }

    #[test]
    #[should_panic]
    fn test_failure_join_periodic() {
        let timer = Timer::new();
        timer
            .schedule_repeating(Duration::from_millis(100), move || {})
            .join();
    }
}
